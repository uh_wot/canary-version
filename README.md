# canary-version

A simple Python 3 script that appends the commit ID to the Deezloader Remix version.

Made for [@DeezloaderRemixBeta](https://t.me/DeezloaderRemixBeta).

# how 2 setup

- Set this env variable for the commit ID (optional, default is "BETA"): `BUILD_SOURCEVERSION`

# how 2 run

1. Move to the bot directory (hint: cd)

2. Run: `python3 fix_ver.py`
