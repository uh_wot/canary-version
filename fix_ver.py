import os
import re

with open("./app/views/index.ejs", "r", encoding="utf-8") as file:
    html = file.read()

commit_id = os.getenv("BUILD_SOURCEVERSION")
if commit_id:
    print("Commit ID: " + commit_id)
    commit_id = commit_id[:10]
else:
    print("Commit ID not found.")
    commit_id = "BETA"

ver = re.search('<span id="appVersionFallback" hidden>(.*)</span>', html).group(1) + "-" + commit_id

html = re.sub('<span id="appVersionFallback" hidden>.*?</span>',
              '<span id="appVersionFallback" hidden>{}</span>'.format(ver), html, flags=re.DOTALL)

html = html.replace('<span id="application_version"></span>', ver, 1)

with open("./app/views/index.ejs", "w", encoding="utf-8") as file:
    file.write(html)
